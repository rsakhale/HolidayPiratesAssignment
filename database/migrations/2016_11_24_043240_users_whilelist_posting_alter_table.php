<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class UsersWhilelistPostingAlterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->boolean('whitelist_posting')->nullable()->after('remember_token');
            $table->timestamp('whitelisted_at')->nullable();
        });
        Schema::create('blacklists', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('reason', 255);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn(['whitelist_posting', 'whitelisted_at']);
        });
        Schema::drop('blacklists');
    }
}
