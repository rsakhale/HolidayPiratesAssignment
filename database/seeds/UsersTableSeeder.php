<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = bcrypt('Welcome@123');
        DB::table('users')->insert([
            'name' => 'Rohan Sakhale',
            'email' => 'rohansakhale@gmail.com',
            'password' => $password,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        DB::table('users')->insert([
            'name' => 'Vivien Schwarz',
            'email' => 'holidaypirates-jobs@m.personio.de',
            'password' => $password,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        factory(App\User::class, 50)->create()->each(function ($u) {
            $numberOfJobs = rand(1, 5);
            for ($i = 0; $i < $numberOfJobs; $i++) {
                $u->jobs()->save(factory(App\Job::class)->make());
            }
        });
    }
}
