<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert([
            'user_id' => 2,
            'title' => 'PHP Developer at HolidayPirates GmbH',
            'description' => 'PHP backend developer',
            'user_id' => 1,
            'approved' => true,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        DB::table('jobs')->insert([
            'user_id' => 1,
            'title' => 'UI Developer at HolidayPirates GmbH',
            'description' => 'Excellent UI Designer to full in our crispy gap',
            'user_id' => 2,
            'approved' => true,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
    }
}
