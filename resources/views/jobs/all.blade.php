@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2>Jobs Listing <a href="{{ url("post-job") }}" class="btn btn-danger pull-right">Post New Job</a></h2>
            <ul class="list-group">
                @foreach($jobs as $job)
                <li class="list-group-item">
                    <a href="{{ $job->path() }}">
                        {{ $job->title }}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection
