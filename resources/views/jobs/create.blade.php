@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<h2>Post a New Job <a class="btn btn-danger pull-right" href="{{ url('jobs-listing') }}">View Jobs Listing</a></h2>
			@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			@if(Session::has('success'))
				<div class="alert alert-success">
					{{ Session::get('success') }}
				</div>
			@endif
			<form action="{{ url('save') }}" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<label>Job Title</label>
					<input type="text" class="form-control" name="title" id="title" value="{{ old("title") }}" />
				</div>
				<div class="form-group">
					<label>Job Description</label>
					<textarea class="form-control" name="description" id="description">{{ old("description") }}</textarea>
				</div>
				<div class="form-group">
					<label>Job Author</label>
					<p class="form-static-control">{{ Auth::user()->name }}</p>
				</div>
				<button type="submit" class="btn btn-primary">
				Save Job
				</button>
			</form>
		</div>
	</div>
</div>
@stop
