<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jobs = Job::all();
        return view('jobs.all', compact('jobs'));
    }

    public function view(Job $job)
    {
        return view('jobs.view', compact('job'));
    }

    public function approve()
    {
        $jobs = Job::where('approved', 0)->get();
        return view('jobs.approve', compact('jobs'));
    }

    public function create()
    {
        return view('jobs.create');
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:jobs',
            'description' => 'min:10',
        ]);
        $approved = Auth::user()->whitelist_posting;
        if ($approved) {
            $msg = "Job details was added successfully";
        } else {
            $msg = "Your job post is awaiting moderator approval";
        }
        $job = new Job(request()->all());
        $job->approved = $approved;
        $job->user_id = Auth::id();
        if ($job->save()) {
            $request->session()->flash('success', $msg);
        }

        return back();
    }
}
